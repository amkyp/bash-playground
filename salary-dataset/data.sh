##################################################
#   ToDo
#       -   Input verification, e.g. ./data.sh 124 4321 <456 - ./data.sh iran tehran =>250
#       -   parsing redirection operators as string and validate them
#       -   tell the user in the first run to enter input in double quotes
#       -   checking if the whole code is running or not
#       -   check this out: https://superuser.com/questions/747884/how-to-write-a-script-that-accepts-input-from-a-file-or-from-stdin
##################################################




#!/bin/bash
##################################################
# SUDO Priviledge
# Check priviledge
##################################################
if [[ ! -w /etc/shadow ]]
then
    echo -e "\033[1;31m[Access Denied] Run With Sudo Privilege.\033[0m" >&2
    exit 1
fi

##################################################
# Input Validation
##################################################
ARG_COUNT=0
for POSITIONAL_ARGUMENTS in "${@}"
do
        ((ARG_COUNT++))
        ARGUMENT_ARRAY[$ARG_COUNT]=$POSITIONAL_ARGUMENTS
        echo "${ARGUMENT_ARRAY[$ARG_COUNT]}" >&2
done





if [[ ! -n $1 ]] || [[ ! -n $2 ]] || [[ ! -n $3 ]]
then
    echo -e "\033[1;31m[RUNTIME ERROR] Invalid Command.\033[0m" >&2
    exit 1
fi

##################################################
#	Constant
##################################################
degree="`echo $2`"
country="`echo $1`"
user_information="`w`"
salary="`echo $3 | tr '[:lower:]' '[:upper:]'`"
Desktop_directory="`echo "/home/$USER/Desktop/"`"
Document_directory="`echo "/home/$USER/Documents"`"
income_log="`echo "/home/$USER/Documents/income.log"`"
Result_income="`echo "/home/$USER/Desktop/Result.income"`"
dataset="`echo adult.data`"

##################################################
# adult.data
# check Database Existence
##################################################
if [[ ! -e $dataset ]]
then
    echo -e "\033[1;33m[RUNTIME ERROR] Load adult.data in current directory in order to not get malfunction\033[0m" >&2
    exit 1
fi

##################################################
# Documents Directory
# check Documents Existence
##################################################
if [[ ! -d $Document_directory ]]
then
    mkdir -p $Document_directory
    touch $income_log
    chown $USER:$USER $income_log
fi

##################################################
# income.file
# check Existence
##################################################
if [[ ! -e $income_log ]]
then
    touch $income_log 
    chown $USER:$USER $income_log 
fi

##################################################
# income.file
# check ivalid ownership
##################################################
if [[ `ls -lg  $income_log | cut -d" " -f 3` != `echo $USER`   ]]
then
    > $income_log 
    chown $USER:$USER $income_log 
fi

##################################################
# Desktop Directory
# check Desktop Existence
##################################################
if [[ ! -d $Desktop_directory ]]
then
    mkdir -p $Desktop_directory
    touch $Result_income
    chown $USER:$USER $Result_income
fi

##################################################
# Result.income
# check Existence
##################################################
if [[ ! -e /home/$USER/Documents/income.log ]]
then
    touch $Result_income
    chown $USER:$USER $Result_income
fi

##################################################
# Result.income
# check ivalid ownership
##################################################
if [[ `ls -lg $Result_income | cut -d" " -f 3` != `echo $USER`   ]]
then
    > $Result_income
    chown $USER:$USER $Result_income
fi

##################################################
#	Constant
##################################################
modified_temp="`echo `stat "$Result_income" | grep "^Modify" | awk '{print $0}'``"
modified_date="`echo "$modified_temp" | cut -d" " -f 1`" # Output -> Date 
modified_time="`echo "$modified_temp" | cut -d" " -f 3`" # Output -> with floating point
modified_time="`echo ${modified_time%\.*}`" # Output -> Decimal
count_line="`awk 'END {print NR}' $Result_income`"
count_words="`wc -w $Result_income | cut -d" " -f 1`"
list_iteration_temp=""

##################################################
## read database
## 
##################################################
## for each time we run script
while IFS= read -r item
do
	# get salary
    item_salary=`echo "$item" | awk '{print $NF}' `
	item_education=`echo "$item" | awk '{print $14}' | cut -d',' -f 1 `
	item_country=`echo "$item" | awk '{print $4}' | cut -d',' -f 1`
	if [[ $item_salary == $salary ]]
	then
		if [[ $item_education == $degree ]]
		then
			if [[ $item_country == $country ]]
			then
				list_iteration_temp=`echo "$list_iteration_temp$item\n"`
			fi
		fi
	fi
done  < <(cat "$dataset" | grep -v "^$")

printf "$list_iteration_temp" >> $Result_income    

log_file="" # 
log_file=`echo -e "$log_file\033[0;32m====================================\033[0m\n"`
log_file=`echo -e "$log_file\033[0;32m## Here is a summary report:\033[0m"`
log_file=`echo -e "$log_file\033[0;32m====================================\033[0m\n"`
log_file=`echo "$log_fileuser:\n"` #  
log_file=`echo "$log_file$user_information:\n"` # user info 
log_file=`echo "${log_file}Modified Date: $modified_date, Modified Time: $modified_time\n"` # time, date, number of lines and words
log_file=`echo "${log_file}Number of lines: $count_line, number of words: $count_words\n"` # time, date, number of lines and words
log_file=`echo -e "$log_file\033[0;32m====================================\033[0m\n"`
log_file=`echo -e "$log_file\033[0;32m====================================\033[0m\n"`

printf "$log_file" >> $income_log

echo -e "\033[0;32m  Have a Good Day\033[0m"
