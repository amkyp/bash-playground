write a backup script named backup.sh.

use `tar` with `gzip` compression to back up the files located inside /etc (including /etc itself and its sub-directories).

The backup file should meet this pattern `backup-YYYY-MM-DD-hh-mm-ss.tar.gz` and should store backups in `/backup` directory.
