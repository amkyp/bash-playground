#!/bin/bash

if [[ ! -d /backup ]]
then
        mkdir /backup
fi


for file in find . -ls | awk '{print $NF}' | sed "s/^.$//g"
do

        backup_time="`date +"%y-%m-%d-%H-%M-%S"`"
        tar -cvf  "backup.tar.gz" "$file"
        sed "s/backup.tar.gz/backup-${backup_time}.tar.gz/g"
        mv "backup-$backup_time.tar.gz" /backup
done

# wrong
# #!/bin/bash 
#   name=$(date '+%Y-%m-%d-%h-%M-%S')
#   tar -zcvf "backup-$name.tar.gz" /etc
#   cp "backup-$name.tar.gz" /backup
#  rm "backup-$name.tar.gz"
